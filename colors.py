import json
from pathlib import Path

import pandas as pd
import pyperclip

COLORS_PER_SCHEME = 10
COLORS_BASE_PATH = Path("~/Downloads/ScientificColourMaps8")

if __name__ == "__main__":
    color_schemes = {}

    # https://docs.python.org/3/library/pathlib.html#pathlib.Path.expanduser
    # https://docs.python.org/3/library/pathlib.html#pathlib.Path.glob
    for hex_file in COLORS_BASE_PATH.expanduser().glob(
        "**/CategoricalPalettes/*_HEX.txt"
    ):
        # print(hex_file)

        df = pd.read_csv(
            hex_file,
            skiprows=2,
            names=["r", "g", "b", "name", "hex"],
            sep="[ \t]+",
            engine="python",
        )
        # print(df)

        color_scheme_name = hex_file.stem.removesuffix("_HEX")
        color_scheme = df.head(COLORS_PER_SCHEME)["hex"].to_list()
        # print(color_scheme)

        color_schemes[color_scheme_name] = color_scheme

    # print(color_schemes)
    pyperclip.copy(json.dumps(color_schemes, ensure_ascii=False, sort_keys=True))
    print("✓ Copied to clipboard!")
