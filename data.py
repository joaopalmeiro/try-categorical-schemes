import pandas as pd
import pyperclip

if __name__ == "__main__":
    df = pd.read_json(
        "https://raw.githubusercontent.com/vega/vega-datasets/v2.7.0/data/barley.json"
    )

    df_to_plot = df.groupby(
        by=["variety", "site"], dropna=False, sort=False, as_index=False
    ).agg({"yield": "sum"})

    # df_to_plot.to_json(
    #     "assets/data.json", orient="records", force_ascii=False, indent=0
    # )
    pyperclip.copy(df_to_plot.to_json(orient="records", force_ascii=False, indent=0))
    print("✓ Copied to clipboard!")
