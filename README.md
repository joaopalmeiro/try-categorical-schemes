# try-categorical-schemes

> https://try-categorical-schemes.vercel.app/

## Development

### Data

```bash
conda env create -f environment.yml
```

```bash
conda activate try-categorical-schemes
```

```bash
python data.py
```

```bash
black data.py
```

### Demo

```bash
fnm install && fnm use && node --version
```

```bash
npm install
```

```bash
npm run dev -- -o
```

## References

- https://d3js.org/d3-scale-chromatic/categorical
- https://vega.github.io/vega/docs/schemes/
- https://jrnold.github.io/ggthemes/reference/tableau_color_pal.html
- https://vega.github.io/vega-lite/examples/stacked_bar_h.html
- https://raw.githubusercontent.com/vega/vega-datasets/v2.7.0/data/barley.json
- https://www.fabiocrameri.ch/colourmaps/ + https://www.fabiocrameri.ch/categorical-colour-maps/
- https://github.com/vitejs/vite/tree/main/packages/create-vite/template-vue-ts
- https://nuxt.com/docs/guide/directory-structure/nuxt
- https://d3-graph-gallery.com/graph/barplot_stacked_basicWide.html
- https://2019.wattenberger.com/blog/d3-interactive-charts
- https://github.com/carbon-design-system/carbon
- https://spectrum.adobe.com/page/color-for-data-visualization/ + https://github.com/adobe/spectrum-css + https://github.com/adobe/spectrum-css/blob/main/components/vars/css/globals/spectrum-colorAliases.css + https://github.com/adobe/spectrum-css/blob/main/components/vars/css/globals/spectrum-colorGlobals.css
- https://cloudscape.design/foundation/visual-foundation/data-vis-colors/
- https://plotly.com/python/discrete-color/ + https://github.com/plotly/plotly.py/blob/v5.15.0/packages/python/plotly/_plotly_utils/colors/qualitative.py
- https://design.gitlab.com/data-visualization/color + https://design.gitlab.com/img/dv-chips-dark-ui.svg + https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com
- https://twitter.com/lisacmuth/status/1491737125821325313
- https://carto.com/carto-colors/ + https://github.com/plotly/plotly.py/blob/v5.15.0/packages/python/plotly/_plotly_utils/colors/carto.py
- https://github.com/salesforce-ux/design-system + https://www.lightningdesignsystem.com/guidelines/data-visualization/charts/#Color

## Notes

- `conda deactivate` + `conda env remove --name try-categorical-schemes`
- `npm install vue && npm install -D @vitejs/plugin-vue typescript vite vue-tsc`
- `npx nuxi@latest init try-categorical-schemes`
- `npm install -D nuxt`
- `npm install -D @nuxtjs/tailwindcss @vueuse/nuxt @vueuse/core`
- `npm install d3-scale d3-scale-chromatic d3-array && npm install -D @types/d3-scale @types/d3-scale-chromatic @types/d3-array`
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/group
- https://www.hyperui.dev/blog/how-to-write-better-containers-in-tailwindcss
- https://www.tempertemper.net/blog/scroll-bounce-page-background-colour
- https://www.photopea.com/ (it opens Adobe XD files)
- https://www.thehyve.nl/articles/open-source-software-licenses-part-2
- https://learnvue.co/articles/vue-drag-and-drop
- https://vercel.com/docs/cli/deploying-from-cli
- https://vercel.com/docs/cli:
  - `npm i -D vercel`
  - Build Command: `npm run generate`
  - Install Command: `npm install`
  - Development Command: `npm run dev`
