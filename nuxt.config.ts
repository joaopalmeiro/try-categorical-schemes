// https://nuxt.com/docs/api/configuration/nuxt-config
// https://devtools.nuxtjs.org/guide
// https://tailwindcss.nuxtjs.org/getting-started/setup

export default defineNuxtConfig({
  modules: ["@nuxtjs/tailwindcss", "@vueuse/nuxt"],
  tailwindcss: {
    viewer: false,
  },
});
